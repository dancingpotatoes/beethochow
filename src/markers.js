var CRIME_MARKERS = {
    'AGG ASSAULT': {
        label: 'Aggravated Assault',
        prefix: 'AA'
    },
    'AUTO THEFT': {
        label: 'Auto Theft',
        prefix: 'AT'
    },
    'BURGLARY-NONRES': {
        label: 'Non-Residential Burglary',
        prefix: 'BN'
    },
    'BURGLARY-RESIDENCE': {
        label: 'Residential Burglary',
        prefix: 'BNR'
    },
    'LARCENY-FROM VEHICLE': {
        label: 'Vehicular Larceny',
        prefix: 'LV'
    },
    'LARCENY-NON VEHICLE': {
        label: 'Non-Vehicular Larceny',
        prefix: 'LNV'
    },
    'HOMICIDE': {
        label: 'Homicide',
        prefix: 'HC'
    },
    'RAPE': {
        label: 'Rape',
        prefix: 'RA'
    },
    'ROBBERY-PEDESTRIAN': {
        label: 'Robbery - Pedestrian',
        prefix: 'RP'
    },
    'ROBBERY-RESIDENCE': {
        label: 'Robbery - Residence',
        prefix: 'RR'
    },
    'ROBBERY-COMMERCIAL': {
        label: 'Robbery - Commercial',
        prefix: 'RC'
    }
}