var MONTH_MARKERS = {
    1: {
        label: 'January',
        color_prefix: 'BLUE',
        color_code: 'blue'
    },
    2: {
        label: 'February',
        color_prefix: 'BROWN',
        color_code: 'rosybrown'
    },
    3: {
        label: 'March',
        color_prefix: 'DARK_GREEN',
        color_code: 'green'

    },
    4: {
        label: 'April',
        color_prefix: 'GREEN',
        color_code: 'lightgreen'
    },
    5: {
        label: 'May',
        color_prefix: 'MAGENTA',
        color_code: 'purple'
    },
    6: {
        label: 'June',
        color_prefix: 'ORANGE',
        color_code: 'orange'
    },
    7: {
        label: 'July',
        color_prefix: 'PALE_BLUE',
        color_code: 'lightblue'
    },
    8: {
        label: 'August',
        color_prefix: 'PINK',
        color_code: 'pink'
    },
    9: {
        label: 'September',
        color_prefix: 'PURPLE',
        color_code: 'magenta'
    },
    10: {
        label: 'October',
        color_prefix: 'RED',
        color_code: 'red'
    },
    11: {
        label: 'November',
        color_prefix: 'WHITE',
        color_code: 'white'
    },
    12: {
        label: 'December',
        color_prefix: 'YELLOW',
        color_code: 'yellow'
    }
}